//    Implement a loop to access and log the city and country of each individual in the dataset.

// Define function
function getAllCityAndCountry(personData) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }
    
    let allCityAndCountry = [];

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        allCityAndCountry.push(personData[personIndex].city, personData[personIndex].country)
    }
    return allCityAndCountry;
}

// export function
module.exports = { getAllCityAndCountry };