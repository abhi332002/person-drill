//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

//Define function 
function getPersonNameByCondition(personData) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        if (personData[personIndex].isStudent == true && personData[personIndex].country == 'Australia') {
            let personName = personData[personIndex].name;
            return personName;
        }
    }
    return null;  //if condition not matched
}

//export the function
module.exports = { getPersonNameByCondition };