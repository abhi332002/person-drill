//   Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

//Define function 
function getAllHobbiesByAge(personData, inputAge) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }
    
    let hobbies = [];

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        if (personData[personIndex].age == inputAge) {
            hobbies = personData[personIndex].hobbies;
        }
    }

    //return hobbies
    return hobbies;
}

module.exports = { getAllHobbiesByAge };