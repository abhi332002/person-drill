//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

//Define function 
function getPersonByPosition(personData, position) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }

    let personName, personCity;

    if (position < personData.length && position > 0) {
        //get person name and city by specific position
        personName = personData[position-1].name;
        personCity = personData[position-1].city;
    } else {
        return null;
    }


    return { personName, personCity };
}

// export function
module.exports = { getPersonByPosition };