//    Write a function that accesses and prints the names and email addresses of individuals aged 25.

//Define function 
function getPersonByAge(personData, inputAge) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }

    let personName, personEmail = null;

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {

        //first match age 
        if (personData[personIndex].age == inputAge) {
            //get person name and email by specific age
            personName = personData[personIndex].name;
            personEmail = personData[personIndex].email;
            return { personName, personEmail };
        }

    }
    return null;



}

// export function
module.exports = { getPersonByAge };