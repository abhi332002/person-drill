//   Implement a loop to access and print the ages of all individuals in the dataset.

//Define function
function getAllPersonAge(personData) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }

    let allPersonAge = [];

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        allPersonAge.push(personData[personIndex].age);
    }

    //return all Person Age 
    return allPersonAge;
}

//export the function 
module.exports = { getAllPersonAge };