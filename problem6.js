//   Create a function to retrieve and display the first hobby of each individual in the dataset.

//Define function 
function getFirstHobbiesOfAllPersons(personData) {

    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }
    
    let firstHobby = [];

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        firstHobby.push(personData[personIndex].hobbies[0]);  //get first hobby to access 0th index
    }

    //first hobby
    return firstHobby;
}

// export function
module.exports = { getFirstHobbiesOfAllPersons };