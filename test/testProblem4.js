const { personData } = require("../personDetails");
const { getPersonByPosition } = require("../problem4");


let personObj = getPersonByPosition(personData, 3);

if (personObj) {
    console.log(`Person name is ${personObj.personName} and city is ${personObj.personCity}`);
} else {
    console.log("person not found");
}