const { personData } = require("../personDetails");
const { getFirstHobbiesOfAllPersons } = require("../problem6");


let firstHobby = getFirstHobbiesOfAllPersons(personData);

if (firstHobby) {
    console.log(firstHobby);
} else {
    console.log("Hobby not found");
}