const { personData } = require("../personDetails");
const { getPersonNameByCondition } = require("../problem3");


let personName = getPersonNameByCondition(personData);

if (personName == null) {
    console.log("person doesn't exists in dataset");
} else {
    console.log("Person name is ", personName);
}