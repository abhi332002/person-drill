const { personData } = require("../personDetails");
const { getAllPersonEmails } = require("../problem1");


let allEmails = getAllPersonEmails(personData);

if (allEmails) {
    console.log(allEmails);
} else {
    console.log("Emails not found!");
}