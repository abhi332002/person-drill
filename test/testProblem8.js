const { personData } = require("../personDetails");
const { getAllCityAndCountry } = require("../problem8");


let addresses = getAllCityAndCountry(personData);

if (addresses) {
    console.log(addresses);
} else {
    console.log("Address not found");
}