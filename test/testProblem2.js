const { personData } = require("../personDetails");
const { getAllHobbiesByAge } = require("../problem2");


let hobbies = getAllHobbiesByAge(personData, 33);

if (hobbies) {
    console.log(hobbies);
} else {
    console.log("Hobbies not found");
}