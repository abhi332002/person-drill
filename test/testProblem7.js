const { personData } = require("../personDetails");
const { getPersonByAge } = require("../problem7");


let person = getPersonByAge(personData, 9);

if (person) {
    console.log(`Person name is ${person.personName} and email is ${person.personEmail}`);
}else {
    console.log('Person details not found');
}