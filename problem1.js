//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.


//Define function 
function getAllPersonEmails(personData) {
 
    //it will check given array is array or not and it's length is greater than 0
    if (!Array.isArray(personData) || personData.length === 0) {
        console.log("Invalid person Data. Please provide a non-empty array.");
        return []; // Return an empty array if personData is invalid or empty
    }

    let emails = [];

    for (let personIndex = 0; personIndex < personData.length; personIndex++) {
        emails.push(personData[personIndex].email);
    }

    //return all emails 
    return emails;
}

//export the function 
module.exports = { getAllPersonEmails };